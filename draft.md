# Abstract

Cash Canary is ...



# Motivation


Secure immutable claims is useful, for example to...

* ..
* ..

## Use Cases

### Generalized Revocation

The **Cash Canary** protocol is based on public key cryptography, and the most generalized claim on can do with a public key is assert that it will no longer be used. When doing so, all interactions with the key should end and users should contact the key holder for further instructions on how to interact in the future.

### Specialized Revocation

In the cryptocurrency economy, a more specialized revocation process can be used where instead of ending all possible interactions with a key, only some aspect is intended to be revoked.

For example, you might not want people to send you money to a specific address anymore, but you still want people to consider the address yours - like when you're changing to a new wallet.

### Charity Corruption Claims

When running a charity organization in a hostile part of the world, there is a significant risk of corruption. By maintaining a hidden canary it is possible to create a situation where corruption by default leads to a loss of trust and therefor income.

It is beneficial to the safety of charity workers that there exist a system that devalues the benefit of corruption, as that in and of itself reduces the incentives and spread of corruption.

### Privacy Trust Breaches

When running a privacy oriented organization where people trust you with their personal information...

### Mutual Risk Agreements

When large amounts of money is involved in a private physical trade, there is a certain degree of risk. When one or more of the participants of the trade are anonymous this risk is enhanced.

If both parties are willing disclose their identities to eachother however, it might be possible to set up a canary that reveals their identities and details of their arrangements to the public unless they both cooperate to invalidate the canary before it activates.

Having a mutual risk in this case, increases the security of both parties by reducing the incentives of either party to commit fruad or acts of violence.


# Specification

...

## Pending Requirements

* **Secret**: It must be possible for a user to have a canary without others being able to detect it.
* **Provable**: Once a canary has been triggered, there can not be any doubt as to who set it off.
* **Immutable**: Once a canary has been triggered, it must be impossible to remove or cancel it.
* **Discoverable**: Detecting a triggered canary must be accessible to the general public.
* **Disconnected**: It must be possible for a user to trigger a canary after losing control of the keys that created it.
* **Granular**: It must be possible to create canaries for a various range of purposes.
* **Scalable**: It must be possible create and detect canaries at a very low cost, or none at all.
* **


## Overall structure?

### Hidden Canaries

The **primary** keypair signs a claim. The **secondary** keypair encrypts the claim and stores it in an OP_RETURN transaction.

The encrypted claim is decrypted by the public key (not address) of the **secondary** keypair so that when the funds are spent the decryption key is automatically revealed and both the claim and the activation of the claim happen together.


### Public canaries

If the claim should be automatically activated if not maintained, the transaction is created with an anyone-can-spend output and nLocktime and the claim itself is not encrypted.

When the output is spent the canary is considered activated. To prevent it from activate, the user spends the same UTXO making the claim unable to be activated.

If the replacing spend is itself is a similar canary, the canary is considered extended in time. If the spend does not contain a similar canary it is to be activated?


### Third party services

...


